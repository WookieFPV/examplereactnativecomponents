/* eslint-disable global-require, import/no-extraneous-dependencies */

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

jest.mock('react-native-camera', () => 'RNCamera');

jest.mock('react-native-i18n', () => {
  const i18njs = require('i18n-js');
  const en = {
    welcome: 'Hello world',
  };
  i18njs.translations = { en };

  return {
    t: jest.fn((k, o) => i18njs.t(k, o)),
    locale: 'en_EN',
  };
});
