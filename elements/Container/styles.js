import { StyleSheet } from 'react-native';

export const hitSlop = {
  top: 5,
  left: 5,
  bottom: 5,
  right: 5,
};

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
});
