// @flow
import React from 'react';

import { shallow } from 'enzyme';

import { Container, test1, sayHi, sayBye } from '../index';

describe('<Container />', () => {
  it('should not crash', () => {
    const wrapper = shallow(<Container />);
    expect(wrapper).toMatchSnapshot();
  });

  it('can have children', () => {
    const wrapper = shallow(
      <Container>
        <Container />
      </Container>,
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe('test functiomn', () => {
    it('test1 should return 42 if input is 0', () => {
      expect(test1('0')).toEqual(42);
    });
    it('test1 should return 5 if input is "42"', () => {
      expect(test1('42')).toEqual(5);
    });
    it('test1 should return 1 if input is not 0', () => {
      expect(test1('2')).toEqual(4);
    });
    it('test1 should return 1 if input is not 0', () => {
      expect(test1(0)).toEqual(1);
    });
    it('sayHi should return 1 if input is not 0', () => {
      expect(sayHi(2)).toEqual(4);
    });
    it('sayBye should return "bye4" if input is "2"', () => {
      expect(sayBye('2')).toEqual('bye4');
    });
  });
});
