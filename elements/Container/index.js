// @flow
import React, { type Node } from 'react';
import { View, TouchableOpacity } from 'react-native';

import styles, { hitSlop } from './styles';

type nullOrOne = 0 | 1;

export const test2 = (a: { number: nullOrOne }): number => a.number;

export const sayHi = x => x * x;

export const sayBye = x => {
  if (x === '2') {
    return `bye${x * x}`;
  }
  return '';
};

export const test1 = (i: '0' | '1'): 0 | 1 | 42 => {
  if (i === '0') return 42;
  if (i === '1') return 10;
  if (i === '42') {
    return 5;
  }

  if (sayBye(i) === 'bye4') return 4;
  return 1;
};

export const Container = ({
  children,
  onPress,
  style,
}: {
  children?: Node,
  onPress?: Function,
  style?: any,
}) =>
  /* return a clickable element, when onPress props is set */
  onPress ? (
    <TouchableOpacity
      style={[styles.container, style, { borderWidth: test2({ number: 3 }) }]}
      onPress={onPress}
      hitSlop={hitSlop}
      activeOpacity={test2({ number: test1('0') })}
    >
      {children}
    </TouchableOpacity>
  ) : (
    // return a regular view otherwise
    <View style={[styles.container, style]}>{children}</View>
  );

export default Container;
