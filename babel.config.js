const fs = require('fs');
const path = require('path');

// require won't load a json file without extension so this is a simple custom module loader
const loadJSON = file => {
  const data = fs.readFileSync(path.resolve(__dirname, file));
  return JSON.parse(data);
};

// ###########################################################
// Babel config used by JEST,
// because it doesn't import the config from .babelrc
// and the metro bundler won't import from babel.config.js
// ###########################################################
module.exports = api => {
  api && api.cache(false);
  // eslint-disable-next-line global-require
  const babelrc = loadJSON('.babelrc');
  return babelrc;
};
